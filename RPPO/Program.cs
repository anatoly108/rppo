﻿using System;
using System.Drawing.Drawing2D;
using System.Linq;

namespace RPPO
{
    class Program
    {
        static void Main(string[] args)
        {
            const string matrix1String = "2 3, 3 4, 1 2";
            const string matrix2String = "2 3, 3 4, 1 2";

//            var stringMatrices = Console.ReadLine();
//            var matrices = stringMatrices.Split(';');
//            var matrix1String = matrices[0];
//            var matrix2String = matrices[1];

            var matrix1 = ConvertStringToMatrix(matrix1String);
            var matrix2 = ConvertStringToMatrix(matrix2String);

            matrix1.Multiply(matrix2);

            var result = ConvertMatrixToString(matrix1);
            var columns = result.Split(',');

            Console.WriteLine(" " + columns[0]);
            Console.WriteLine(columns[1]);
            Console.WriteLine(columns[2]);
        }

        private static Matrix ConvertStringToMatrix(string matrix)
        {
            var m1Lines = matrix.Split(',');
            var m1Values = m1Lines.SelectMany(x => x.Split(' ')).Where(x => x!="");
            var values = m1Values.Select(int.Parse).ToArray();
            var m11 = values[0];
            var m12 = values[1];
            var m21 = values[2];
            var m22 = values[3];
            var dx = values[4];
            var dy = values[5];
            return new Matrix(m11, m12, m22, m21, dx, dy);
        }

        private static string ConvertMatrixToString(Matrix matrix)
        {
            var result = "";
            result += matrix.Elements[0] + " " + matrix.Elements[1];
            result += " , " + matrix.Elements[2] + " " + matrix.Elements[3];
            result += " , " + matrix.Elements[4] + " " + matrix.Elements[4];

            return result;
        }
    }
}
